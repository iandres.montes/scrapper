# -*- coding: utf-8 -*-

from time import sleep
from bs4 import BeautifulSoup
from helpers.item import Item
from helpers.amazon import Amazon
from tqdm import tqdm
file = open('source.csv', 'w', encoding='utf-8')

file.writelines('id,asin,url,title, price, status\n')
amazon = Amazon()


def write(product, item):
    file.writelines(f'{product.id},{product.asin},{product.url},{item.title},{item.price},{status}\n')
    return True


for product in tqdm(amazon.build_urls()):
    sleep(10)
    soup = amazon.get(product)
    item = Item(soup)
    status = 1 if item.price != 0 else 0
    write(product, item)

file.close()